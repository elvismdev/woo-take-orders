## Woo Take Orders ##

Application para colocar ordenes en Woocommerce fuera de Wordpress usando la WooCommerce REST API

Esta aplicacion es para usuarios que son agentes de venta por telefono para que le permita poner ordenes de la tienda online para el cliente con el que esta hablando. Seria un backend en symfony que se comunivca con wordpress utilizando su API en http://woothemes.github.io/woocommerce-rest-api-docs/. Lo cual seria tomar la informacion de su tarjeta de credito procesar el pago con Paypal utilizando algun Paypal API Bundle en symfony si es posible.