<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PaymentControllerTest extends WebTestCase
{
    public function testPrepareauthorizenetpayment()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/payment_prepare');
    }

    public function testCaptureauthorizenetdone()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/payment_done');
    }

}
