<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CartContent;
use AppBundle\Entity\Customer;
use AppBundle\Form\CustomerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * CartContent controller.
 *
 * @Route("/cart")
 */
class CartContentController extends Controller
{
    /**
     * Lists all User cart products.
     *
     * @Route("/", name="cart")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entities = $em->getRepository('AppBundle:CartContent')->findByUser($user);

        // Call WooClient Service defined in WooBundle's service.yml file
        $wooclient = $this->get('woo.client');

        foreach ($entities as $entity) {
            $p = $wooclient->products->get($entity->getProductId());
            $p = $p->product;

            $entity->title = $p->title;
            $entity->price = $p->sale_price;
            $entity->subtotalprice = $p->sale_price * $entity->getQuantity();
        }

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Deletes a User's cart products.
     *
     * @Route("/remove", name="cart_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $products = $request->request->get('delFromCart', '');

        if ($products) {
            $products = explode(',', $products);

            foreach ($products as $product) {
                $cart = $em->getRepository('AppBundle:CartContent')->findOneBy(array('user' => $user, 'id' => (int)$product));

                if ($cart) {
                    $em->remove($cart);
                }
            }

            $em->flush();
        }

        return $this->redirect($this->generateUrl('cart'));
    }

    /**
     * Deletes all User's cart products.
     *
     * @Route("/clear", name="cart_clear")
     * @Method("GET")
     */
    public function clearAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em->getRepository('AppBundle:CartContent')->createQueryBuilder('c')
            ->delete('AppBundle:CartContent', 'c')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()->execute();

        return $this->redirect($this->generateUrl('cart'));
    }

    /**
     * Checkout products.
     *
     * @Route("/checkout", name="cart_checkout")
     * @Method("GET")
     * @Template()
     */
    public function checkoutAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $customer = new Customer();

        // If previously non success checkout
        $session = $this->get('session');
        if ($session->has('checkout_details')) {
            $details = $session->get('checkout_details')->getDetails();

            $customer->setFirstname($details['first_name']);
            $customer->setLastname($details['last_name']);
            $customer->setAddress($details['address']);
            $customer->setCity($details['city']);
            $customer->setCompany($details['company']);
            $customer->setCountry($details['country']);
            $customer->setEmail($details['email']);
            $customer->setState($details['state']);
            $customer->setZip($details['zip']);

            $customer->setCcard($details['card_num']);
            $customer->setCcardsecuritycode($details['card_code']);
        }

        // Client Form
        $form = $this->createForm(new CustomerType(), $customer, array(
            'action' => $this->generateUrl('payment_prepare'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        // Cart summary
        $entities = $em->getRepository('AppBundle:CartContent')->findByUser($user);

        // Call WooClient Service defined in WooBundle's service.yml file
        $wooclient = $this->get('woo.client');

        foreach ($entities as $entity) {
            $p = $wooclient->products->get($entity->getProductId());
            $p = $p->product;

            $entity->title = $p->title;
            $entity->price = $p->sale_price;
            $entity->subtotalprice = $p->sale_price * $entity->getQuantity();
        }

        return array(
            'entities' => $entities,
            'form' => $form->createView(),
        );
    }

    /**
     * Count all Products from logged in User.
     *
     * @Route("/count", name="cart_count")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function countProductsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $count = $em->getRepository('AppBundle:CartContent')->createQueryBuilder('c')
            ->select('count(c.id)')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('@App/CartContent/cart.html.twig', array(
            'count' => $count,
        ));
    }

    /**
     * Receive Products ID (WooCommerce WP) and put them into Cart
     *
     * @Route("/add", name="cart_add")
     * @Method("POST")
     *
     */
    public function addToCartAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $products = $request->request->get('toCart', '');

        if ($products) {
            $products = explode(',', $products);

            foreach ($products as $product) {
                $cart = $em->getRepository('AppBundle:CartContent')->findOneBy(array('user' => $user, 'productid' => (int)$product));

                if ($cart) {
                    $cart->addQty();
                } else {
                    $cart = new CartContent();

                    $cart->setProductid((int)$product);
                    $cart->setQuantity(1); // Default Qty
                    $cart->setUser($user);
                }

                $em->persist($cart);
            }

            $em->flush();
        }

        return $this->redirect($this->generateUrl('product'));
    }

    /**
     * Modify cart products Qty
     *
     * @Route("/update", name="cart_update")
     * @Method("POST")
     *
     */
    function updateCartAction(Request $request)
    {
        $qtys = $request->request->get('qty', array());

        if (!empty($qtys)) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.token_storage')->getToken()->getUser();

            foreach ($qtys as $id => $qty) {
                $cart = $em->getRepository('AppBundle:CartContent')->findOneBy(array('user' => $user, 'id' => (int)$id));

                if (!$cart)
                    continue;

                $cart->modQty((int)$qty);

                $em->persist($cart);
            }

            $em->flush();
        }

        return $this->redirect($this->generateUrl('cart'));
    }
}
