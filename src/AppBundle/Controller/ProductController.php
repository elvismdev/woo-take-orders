<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Product controller.
 *
 * @Route("/products")
 */
class ProductController extends Controller
{
    /**
     * Lists all Products from Woo Store.
     *
     * @param int $page
     *
     * @Route("/{page}", defaults={"page" = 1}, name="product")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function indexAction($page)
    {
        // Call WooClient Service defined in WooBundle's service.yml file
        $wooclient = $this->get('woo.client');

        // Total of products pages
        $pages = ceil(floatval($wooclient->products->get_count()->count / 10));

        // Get page products
        $products = $wooclient->make_api_call("GET", "products", array('page' => $page));

        return array(
            'products' => $products->products,
            'page' => $page,
            'pages' => $pages
        );
    }
}
