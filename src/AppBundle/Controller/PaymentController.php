<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Form\CustomerType;
use Payum\Core\Security\SensitiveValue;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Payum\Core\Request\GetHumanStatus;

/**
 * PaymentController controller.
 *
 * @Route("/payment")
 */
class PaymentController extends Controller
{
    /**
     * @Route("/prepare", name="payment_prepare")
     * @Method("POST")
     * @Template("@App/CartContent/checkout.html.twig")
     */
    public function prepareAuthorizeNetPaymentAction(Request $request)
    {
        $customer = new Customer();
        $form = $this->createNewForm($customer);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.token_storage')->getToken()->getUser();

            // Cart summary
            $entities = $em->getRepository('AppBundle:CartContent')->findByUser($user);

            // Call WooClient Service defined in WooBundle's service.yml file
            $wooclient = $this->get('woo.client');

            foreach ($entities as $entity) {
                $p = $wooclient->products->get($entity->getProductId());
                $p = $p->product;

                $entity->title = $p->title;
                $entity->price = $p->sale_price;
                $entity->subtotalprice = $p->sale_price * $entity->getQuantity();
            }

            return array(
                'entities' => $entities,
                'form' => $form->createView(),
            );
        }

        $checkout = $request->request->get('appbundle_customer', array());
        if (!$checkout)
            return $this->redirect($this->generateUrl('cart_checkout'));

        $gatewayName = 'authorizeNet';

        $storage = $this->get('payum')->getStorage('AppBundle\Entity\PaymentDetails');

        /** @var \AppBundle\Entity\PaymentDetails $details */
        $details = $storage->create();

        // Card and money
        $details['amount'] = floatval($checkout['amount']);
        $details['card_num'] = $checkout['ccard'];
        $details['exp_date'] = str_pad(substr($checkout['ccardexp']['year'], -2), 2, '0', STR_PAD_LEFT) . str_pad($checkout['ccardexp']['month'], 2, '0', STR_PAD_LEFT);
        $details['card_code'] = $checkout['ccardsecuritycode'];

        // Client
        $details['first_name'] = $checkout['firstname'];
        $details['last_name'] = $checkout['lastname'];
        $details['email'] = $checkout['email'];
        $details['company'] = $checkout['company'];
        $details['address'] = $checkout['address'];
        $details['city'] = $checkout['city'];
        $details['state'] = $checkout['state'];
        $details['zip'] = $checkout['zip'];
        $details['country'] = $checkout['country'];

        $storage->update($details);

        // Details in case of non success
        $session = $this->get('session');
        $session->set('checkout_details', $details);
        $session->save();

        $captureToken = $this->get('payum.security.token_factory')->createCaptureToken(
            $gatewayName,
            $details,
            'payment_done' // the route to redirect after capture
        );

        return $this->redirect($captureToken->getTargetUrl());
    }

    /**
     * @Route("/done", name="payment_done")
     * @Template()
     */
    public function captureAuthorizeNetDoneAction(Request $request)
    {
        $token = $this->get('payum.security.http_request_verifier')->verify($request);

        $gateway = $this->get('payum')->getGateway($token->getGatewayName());

        $gateway->execute($status = new GetHumanStatus($token));
        $details = $status->getFirstModel()->getDetails();

        //$test = 1;
        if ($status->isCaptured() || $status->isAuthorized() /*|| $test*/) {
            $session = $this->get('session');
            if ($session->has('checkout_details')) {
                $trans_details = $details;
                $details = $session->get('checkout_details')->getDetails();
                $session->remove('checkout_details');
            }

            // I have my customer's data now i can work with it
            $customer['email'] = $details['email'];
            $customer['first_name'] = $details['first_name'];
            $customer['last_name'] = $details['last_name'];
            $customer['username'] = str_replace(' ', '', ctype_lower($details['first_name'] . $details['last_name']));
            $customer['billing_address'] = array(
                'first_name' => $details['first_name'],
                'last_name' => $details['last_name'],
                'company' => $details['company'],
                'address_1' => $details['address'],
                'city' => $details['city'],
                'state' => $details['state'],
                'postcode' => $details['zip'],
                'country' => $details['country'],
                'email' => $details['email'],
            );
            $customer['shipping_address'] = array(
                'first_name' => $details['first_name'],
                'last_name' => $details['last_name'],
                'company' => $details['company'],
                'address_1' => $details['address'],
                'city' => $details['city'],
                'state' => $details['state'],
                'postcode' => $details['zip'],
                'country' => $details['country'],
            );

            // WP WooCommerce API
            $wooclient = $this->get('woo.client');

            // Check if customer alredy exist
            try {
                $woo_customer = $wooclient->customers->get_by_email($customer['email']);
            } catch (\Exception $e) {
                if (strstr($e->getMessage(), 'woocommerce_api_invalid_customer_email')) {
                    // Create customer in WooCommerce
                    try {
                        $woo_customer = $wooclient->customers->create($customer);
                    } catch (\Exception $e) {
                        $this->get('session')->getFlashBag()->add(
                            'notice',
                            'WooCommerce Error creating customer: ' . $e->getMessage()
                        );

                        return $this->redirect($this->generateUrl('cart_checkout'));
                    }
                }
            }

            $woo_customer = $woo_customer->customer;

            // Create order
            $order['payment_details'] = array(
                'method_id' => 'AuthorizeNET',
                'method_title' => 'Credit Card',
                'paid' => true
            );
            $order['billing_address'] = $customer['billing_address'];
            $order['shipping_address'] = $customer['shipping_address'];
            $order['customer_id'] = $woo_customer->id;

            // Items in Cart
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $entities = $em->getRepository('AppBundle:CartContent')->findByUser($user);
            foreach ($entities as $item) {
                $order['line_items'][] = array(
                    'product_id' => $item->getProductid(),
                    'quantity' => $item->getQuantity()
                );
            }

            // Create customer's order in WooCommerce
            try {
                $woo_order = $wooclient->orders->create($order);
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'WooCommerce Error creating order: ' . $e->getMessage()
                );

                return $this->redirect($this->generateUrl('cart_checkout'));
            }

            // Order note
            $woo_order = $woo_order->order;

            $note['note'] = 'Authorized.NET Transaction Approved: ' . isset($trans_details) ? $trans_details["card_type"] : 'Credit Card' . ' ending in ' . substr($details['card_num'], -4) . ' (expires ' . $details['exp_date'] . ')';
            try {
                $woo_order_note = $wooclient->order_notes->create($woo_order->id, $note);
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'WooCommerce Error creating order note: ' . $e->getMessage()
                );
            }

            // Deleting cart products
            return $this->redirect($this->generateUrl('cart_clear'));

            /*return new JsonResponse(array(
                'status' => $status->getValue(),
                'details' => isset($trans_details) ? $trans_details : $details,
            ));*/
        }

        $this->get('session')->getFlashBag()->add(
            'notice',
            ($details['response_code']) ? $details['response'] : $details['error_message']
        );

        return $this->redirect($this->generateUrl('cart_checkout'));
    }

    /**
     * Creates a form to edit a Customer entity.
     *
     * @param Customer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createNewForm(Customer $entity)
    {
        $form = $this->createForm(new CustomerType(), $entity, array(
            'action' => $this->generateUrl('payment_prepare'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

}
