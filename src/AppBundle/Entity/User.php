<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CartContent", mappedBy="user", cascade={"persist", "merge", "remove"})
     */
    private $cartProducts;


    public function __construct()
    {
        parent::__construct();
        $this->cartProducts = new ArrayCollection();
    }

    /**
     * @var string
     */
    protected  $role;

    /**
     * @return string
     */
    public function getOneRole()
    {
        $roles = $this->getRoles();

        return $roles[0];
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * set CartProducts
     *
     * @param ArrayCollection $cartProducts
     */
    public function setCartProducts(ArrayCollection $cartProducts)
    {
        $this->cartProducts[] = $cartProducts;
    }

    /**
     * Get CartProducts
     *
     * @return ArrayCollection
     */
    public function getCartProducts()
    {
        return $this->cartProducts;
    }

    /**
     * add CartProduct
     *
     * @param CartContent $cartProduct
     *
     * @return User
     */
    public function addCartProduct(CartContent $cartProduct = null)
    {
        $this->cartProducts[] = $cartProduct;

        return $this;
    }

    /**
     * Remove CartProduct
     *
     * @param CartContent $cartProduct
     */
    public function removeCartProduct(CartContent $cartProduct)
    {
        $this->cartProducts->removeElement($cartProduct);
    }
}