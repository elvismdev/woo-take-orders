<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

class Customer
{

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=25, minMessage = "Your first name must be at least {{ limit }} characters long", maxMessage = "Your first name cannot be longer than {{ limit }} characters")
     */
    private $firstname;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=45, minMessage = "Your last name must be at least {{ limit }} characters long", maxMessage = "Your last name cannot be longer than {{ limit }} characters")
     */
    private $lastname;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=65, minMessage = "Your company name must be at least {{ limit }} characters long", maxMessage = "Your company name cannot be longer than {{ limit }} characters")
     */
    private $company;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $state;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $zip;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email(checkMX=true, checkHost=true, message = "The email '{{ value }}' is not a valid email.")
     */
    private $email;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\CardScheme(schemes={"VISA", "MASTERCARD", "AMEX", "DISCOVER", "JCB", "DINERS"}, message="Your credit card number is invalid.")
     */
    private $ccard;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=3)
     */
    private $ccardsecuritycode;

    /**
     * @var string
     */
    private $ccardexp;

    /**
     * @var float
     *
     * @Assert\NotBlank()
     */
    private $amount;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Country()
     */
    private $country;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Client
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Client
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Client
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Client
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Client
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Client
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getCcard()
    {
        return $this->ccard;
    }

    /**
     * @param int $ccard
     *
     * @return Customer
     */
    public function setCcard($ccard)
    {
        $this->ccard = $ccard;

        return $this;
    }

    /**
     * @return string
     */
    public function getCcardexp()
    {
        return $this->ccardexp;
    }

    /**
     * @param string $ccardexp
     *
     * @return Customer
     */
    public function setCcardexp($ccardexp)
    {
        $this->ccardexp = $ccardexp;

        return $this;
    }

    /**
     * @return string
     */
    public function getCcardsecuritycode()
    {
        return $this->ccardsecuritycode;
    }

    /**
     * @param string $ccardsecuritycode
     *
     * @return Customer
     */
    public function setCcardsecuritycode($ccardsecuritycode)
    {
        $this->ccardsecuritycode = $ccardsecuritycode;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     *
     * @return Customer
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Client
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
}