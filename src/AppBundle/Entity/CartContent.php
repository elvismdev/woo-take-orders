<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cart_content")
 */
class CartContent {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var productid
     * @ORM\Column(name="productid", type="integer", nullable=false, options={"default" = 0})
     */
    private $productid;

    /**
     * @var quantity
     * @ORM\Column(name="quantity", type="integer", nullable=false, options={"default" = 0})
     */
    private $quantity;

    /**
     * @var \AppBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="cartProducts")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     **/
    private $user;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * @param integer $productid
     * @return CartContent
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param integer $quantity
     * @return CartContent
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Set User
     *
     * @param User $user
     *
     * @return CartContent
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return CartContent
     */
    public function addQty()
    {
        $this->quantity++;

        return $this;
    }

    /**
     * @param integer $qty
     *
     * @return CartContent
     */
    public function modQty($qty)
    {
        if ($this->quantity != $qty && $qty> 0)
            $this->quantity = $qty;

        return $this;
    }

}