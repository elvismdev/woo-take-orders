<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text', array('attr' => array('required' => true, 'maxlength' => 25)))
            ->add('lastname', 'text', array('attr' => array('required' => true, 'maxlength' => 45)))
            ->add('company', 'text', array('attr' => array('required' => true, 'maxlength' => 25)))
            ->add('address', 'text', array('attr' => array('required' => true, 'maxlength' => 25)))
            ->add('city', 'text', array('attr' => array('required' => true, 'maxlength' => 25)))
            ->add('state', 'text', array('attr' => array('required' => true, 'maxlength' => 25)))
            ->add('country', 'country', array('attr' => array('required' => true)))
            ->add('zip', 'integer', array('attr' => array('required' => true, 'maxlength' => 10)))
            ->add('email', 'email', array('attr' => array('required' => true)))
            ->add('ccard', 'text', array('attr' => array('required' => true, 'maxlength' => 20)))
            ->add('ccardexp', 'date', array())
            ->add('ccardsecuritycode', 'integer', array('attr' => array('required' => true, 'maxlength' => 3)))
            ->add('amount', 'hidden')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Customer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_customer';
    }
}
