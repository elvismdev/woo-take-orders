<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @var bool
     */
    private $new;

    /**
     * @param bool $new
     */
    public function __construct($new = true)
    {
        $this->new = $new;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['data'];

        $builder->add('username');

        if ($this->new)
            $builder->add('password', 'repeated', array('required' => true, 'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password')));
        else
            $builder->add('password', 'password', array('required' => false));

        $builder
            ->add('email', 'email')
            ->add('role', 'choice', array('choices' => array('ROLE_ADMIN' => 'Administrator', 'ROLE_SALESGUY' => 'Sales Guy'), 'data' => $user->getOneRole()));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_user';
    }
}
